/*
 * Copyright © 2019 AperLambda <aperlambda@gmail.com>
 *
 * This file is part of λcommon.
 *
 * Licensed under the MIT license. For more information,
 * see the LICENSE file.
 */

#ifndef LAMBDACOMMON_DOCUMENT_H
#define LAMBDACOMMON_DOCUMENT_H

#include "../lambdacommon.h"
#include <map>

namespace lambdacommon
{
	class LAMBDACOMMON_API Element
	{
	};

	class LAMBDACOMMON_API Document
	{
	};
}

#endif //LAMBDACOMMON_DOCUMENT_H
